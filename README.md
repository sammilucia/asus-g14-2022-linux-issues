# ASUS-G14-2022-Linux-Issues

Comprehensive list of all issues on Linux for ASUS G14 (GA402xx) for asus-linux.org.
Note only Fedora and Arch will be supported at this time, other distros are not up to date enough.

# Project goal

The goal of asus-linux.org is that the current/up-to-date Linux distros work fully on all ASUS laptops.

# Contributing

Please feel free to contribute by searching and adding in [Issues](https://gitlab.com/sammilucia/asus-g14-2022-linux-issues/-/issues). Please do the steps in the [Guides](https://asus-linux.org/wiki/) on asus-linux.org, and ask in the [Discord](https://discord.com/invite/4ZKGd7Un5t) as appropriate.

Please be specific and include as much detail as possible, including:
- How to reproduce the issue (if possible)
- What you were doing when the problem occurred (if no way to repro yet)

# Other models

For general issues for other ASUS models, please use https://gitlab.com/asus-linux/general-issue-tracking/. As always check the website and discord first.

# Project status

Currently the 2020 and 2021 G14 notebooks work completely out of the box with Fedora and Arch (Manjaro is not supported). 2022 G14s are usable for production with a few caveats.
